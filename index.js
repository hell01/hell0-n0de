const path = require('path');
const http = require('http');

const express = require('express');
const app = express();

const server = http.createServer(app);
const PORT = process.env.PORT || 8080;

app.use((req, res, next)=> {
	console.log(`Route '${req.url}' is executed;`);
	next();
});

app.get('/name',(req, res)=> {
	res.send({
		fname: 'Suyash',
		lname: 'Kale'
	});
});

app.use(express.static(path.join(__dirname, 'public')));

server.listen(PORT, ()=> {
  console.log(`Server is up and running on port ${PORT};`);
});